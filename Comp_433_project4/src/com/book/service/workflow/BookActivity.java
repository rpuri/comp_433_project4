package com.book.service.workflow;

import com.book.domain.Book;
import com.book.domain.BookDAO;
import com.book.service.representation.BookRepresentation;

public class BookActivity {

	public BookRepresentation getBook(Integer isbn) {
		BookDAO dao = new BookDAO();
		Book book = dao.getBook(isbn);
		BookRepresentation bookrep = new BookRepresentation();
		bookrep.setBookName(book.getBookName());
		bookrep.setcopies(book.getCopies());
		bookrep.setISBN(book.getISBN());
		bookrep.setPrice(book.getPrice());
		return bookrep;
	}
}
