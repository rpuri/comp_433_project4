package com.book.services;

import com.book.place.order.Order;
import com.book.service.representation.BookRepresentation;
import com.book.service.representation.OrderRepresentation;

public interface BookService {
	BookRepresentation getbook(int isbn);
    OrderRepresentation setOrder(OrderRepresentation setorder);
    String getOrderStatus(int orderNum);
    String cancelOrder(int orderNum);
}
